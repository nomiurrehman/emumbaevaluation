class Solution(object):
    def maxDistToClosest(self, seats):
        """
        :type seats: List[int]
        :rtype: int
        """
        max = 0
        curr = 0
        init = True
        for seat in seats:
            if seat == 1:
                if init:
                    max = curr
                    curr = 0
                init = False
                if curr == 0:
                    continue
                curr = (curr - 1) / 2 + 1
                if curr > max:
                    max = curr
                curr = 0
            else:
                curr += 1
        if curr > max:
            max = curr
        return max
