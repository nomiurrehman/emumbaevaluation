class Solution(object):
    def sortArrayByParity(self, A):
        """
        :type A: List[int]
        :rtype: List[int]
        """
        odd = []
        even = []
        for el in A:
            if el % 2 == 0:
                even.append(el)
            else:
                odd.append(el)
        return even + odd
