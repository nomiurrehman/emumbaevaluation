import collections


class Solution:
    def leastInterval(self, tasks, n):
        """
        :type tasks: List[str]
        :type n: int
        :rtype: int
        """
        cnt = collections.Counter(tasks)
        exe = 0
        delay = 0
        while cnt:
            exe = exe + delay
            most = cnt.most_common()
            most_num = len(most)
            delay = 0
            if most_num <= n + 1:
                delay = n + 1 - most_num
                for el in most:
                    exe += 1
                    cnt[el[0]] -= 1
                    if cnt[el[0]] <= 0:
                        del cnt[el[0]]
            else:
                for el in most[:n + 1]:
                    exe += 1
                    cnt[el[0]] -= 1
                    if cnt[el[0]] == 0:
                        del cnt[el[0]]
        return exe
