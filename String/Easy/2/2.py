class Solution:
    def isLongPressedName(self, name, typed):
        """
        :type name: str
        :type typed: str
        :rtype: bool
        """
        if name[0] != typed[0]:
            return False
        num = 1
        total = len(name)
        prev = name[0]
        curr = name[1]
        for char in typed[1:]:
            if curr == char:
                prev = curr
                if num < total:
                    num += 1
                if num < total:
                    curr = name[num]
            elif prev == char:
                continue
            else:
                return False
        if num == total:
            return True
        return False
