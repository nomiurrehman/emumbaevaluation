class Solution:
    def numUniqueEmails(self, emails):
        """
        :type emails: List[str]
        :rtype: int
        """
        for index in range(len(emails)):
            email = emails[index]
            [localname, domain] = email.split('@', 1)
            [initial, _] = localname.split('+', 1)
            uniq = initial.replace('.', '')
            emails[index] = uniq + domain
        return len(set(emails))
