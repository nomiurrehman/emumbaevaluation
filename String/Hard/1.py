import math
class Solution:
    def nearestPalindromic(self, n):
        """
        :type n: str
        :rtype: str
        """
        if len(n)==1:
            return str(int(n)-1)
        higher = self.findPalindrome(n)
        if higher == n:
            higher = self.findPalindrome(str(int(n) + 1))

        lower = self.lower_palindrome(n)
        if lower == n:
            lower = self.lower_palindrome(str(int(n)-1))

        num = int(n)
        ldiff = abs(num - int(lower))
        hdiff = abs(num - int(higher))

        if lower == n:
            return higher
        if higher == n:
            return lower

        if ldiff <= hdiff:
            return lower
        else:
            return higher

    def findPalindrome(self, n):
        count = 0
        mini = min(18, int(len(n) / 2))
        str_list = list(n)
        while count < mini:
            str_list[-count - 1] = str_list[count]
            count += 1
        return str.join('', str_list)

    def lower_palindrome(self, n):
        center = math.ceil(len(n) / 2)
        ini = int(n[:center]) - 1

        if ini:
            n_new = str(ini) + n[center:]
            lower = self.findPalindrome(n_new)
        else:
            lower = '9'
        return lower
