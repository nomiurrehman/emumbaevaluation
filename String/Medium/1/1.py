class Solution:
    def generateParenthesis(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        self.paran = []
        self.recur(n, n, '')
        return self.paran

    def recur(self, left, right, content):
        if not left and not right:
            self.paran.append(content)
        if left:
            self.recur(left - 1, right, content + '(')
        if left > right - 1:
            return
        if right:
            self.recur(left, right - 1, content + ')')
