class Solution:
    def calculate(self, s):
        """
        :type s: str
        :rtype: int
        """
        self.record = []
        value = 0
        token = '+'

        num = ''
        for char in s:
            if char.isdigit():
                num = num + char
            elif char in ['+', '-', '*', '/']:
                value = int(num)
                num = ''
                self.evaluate(token, value)
                token, value = char, 0
        value = int(num)
        self.evaluate(token, value)
        return sum(self.record)


    def evaluate(self, token, value):
        if token == '*':
            self.record.append(self.record.pop() * value)
        elif token == '/':
            l = self.record.pop()
            r = value
            if l * r < 0 and l % r != 0:
                self.record.append(l // r + 1)
            else:
                self.record.append(l // r)
        elif token == '+':
            self.record.append(value)
        elif token == '-':
            self.record.append(-value)
